ANALOG
Ch 1 Scale 20mV/, Pos 3.000mV, Coup DC, BW Limit Off, Inv Off, Imp 1M Ohm
     Probe 1.000 : 1, Skew 0.0s
Ch 2 Scale 100mV/, Pos 82.400mV, Coup DC, BW Limit Off, Inv Off, Imp 1M Ohm
     Probe 1.000 : 1, Skew 0.0s

TRIGGER
Sweep Mode Auto, Coup DC, Noise Rej On, HF Rej On, Holdoff 40.0ns
Mode Edge, Source Ch 1, Slope Rising, Level 18.250mV

HORIZONTAL
Mode Normal, Ref Center, Main Scale 2.000ms/, Main Delay 4.500000000ms

ACQUISITION
Mode Normal, Realtime On, Vectors On, Persistence Off

