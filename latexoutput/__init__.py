"""
LaTeXOutput
===========

authors
-------
    Christian Geister
    Tim Kallage

version
-------
    2017-07-22
"""
import os
import numpy as np
from uncertainties import UFloat
from itertools import zip_longest

_output_level = 0
_output_dir = "build"
__version__ = "2017-07-22"


def _ufloat_to_str(x):
    x_str = "{:u}".format(x)
    x_str = x_str.replace("(", "")
    x_str = x_str.replace(")", "")
    x_str = x_str.replace("/", "")
    return x_str


def _python_fmt(fmt):
    if fmt == "":
        return ""
    bef_dec_point = int(fmt.split(".")[0])
    aft_dec_point = fmt.split(".")[1]
    if "e" in fmt:
        aft_dec_point = int(aft_dec_point.split("e")[0])
        style = "e"
    else:
        aft_dec_point = int(aft_dec_point)
        style = "f"
    total_digits = bef_dec_point
    if aft_dec_point > 0:
        total_digits += 1 + aft_dec_point
    if style == "e":
        total_digits += 3
    py_fmt = "{:d}.{:d}{:s}".format(total_digits, aft_dec_point, style)
    return py_fmt


def latex_table(name, content, col_title="", col_unit="", caption="",
                label="", fmt="", table_env=True, unpack=True,
                output_dir=_output_dir):
    """
    Save multiple arrays/lists to a file as a table LaTeX-Style

    Parameters
    ----------
    name : str
        The name of the table, defines the filename.
        Filename for ``name="x"`` will be `val_x.tex`.
    content : {list of array, 2d-array}
        The data to be displayed in the table.
    col_title : {str, list of str}, optional
        A title for each column can be provided, either as list or a single
        comma-seperated string.
    col_unit : {str, list of str}, optional
        A unit for each column can be provided, either as list or a single
        comma-seperated string. Will be displayed as `[unit]` after the
        column name.
    caption : str, optional
        The LaTeX-table caption, to be shown above the table.
    label : str, optional
        A label for LaTeX-references, defaults to `tbl:name`
    fmt : str, optional
        A number format in siunitx-style for each column as list or a comma-
        seperated string.
    table_env : bool, optional
        If true (default), a LaTeX-`table` environment is generated.
        Required for the caption and label.
    unpack : bool, optional
        If true (default), the input data is transposed. This allows that
        the data is provided as a list of column-arrays.
    output_dir : str, optional
        The directory, in which the .tex file is saved to.
        Defaults to the global variable ``_output_dir``.

    Notes
    -----
        For more information on siunitx, see
        http://mirror.hmc.edu/ctan/macros/latex/contrib/siunitx/siunitx.pdf
    """
    # zip lists to one 2dim array (side effect: swaps axes)
    content = list(zip_longest(*content))
    if unpack is False:
        content = list(map(list, zip(*content)))

    # parse string-lists into lists
    if isinstance(col_title, str):
        col_title = col_title.split(sep=",")
    if isinstance(col_unit, str):
        col_unit = col_unit.split(sep=",")
    if isinstance(fmt, str):
        fmt = fmt.split(sep=",")

    nrows, ncols = np.shape(content)
    # remove leading/trailing spaces and cut off if too long
    col_title = [s.strip() for s in col_title[:ncols]]
    col_unit = [s.strip() for s in col_unit[:ncols]]
    fmt = [s.strip() for s in fmt[:ncols]]
    # extend to ncols if too short
    col_title.extend([""] * (ncols - len(col_title)))
    col_unit.extend([""] * (ncols - len(col_unit)))
    fmt.extend([""] * (ncols - len(fmt)))

    # include col_unit into col_title
    for i in range(ncols):
        if col_unit[i]:
            col_title[i] += " [\\si{" + col_unit[i] + "}]"

    if(label == ""):
        label = "tbl:{}".format(name)
    elif not label.startswith("tbl:"):
        label = "tbl:{}".format(label)

    # latex syntax for a table with caption and label
    str_result = ""
    if table_env:
        str_result += "\\begin{table}[ht]\n"
    str_result += "\\begin{center}\n"
    if table_env:
        str_result += "\\caption{" + caption + "}\n"
        str_result += "\\label{" + label + "}\n"
    # round to specified format value
    str_result += "\\sisetup{table-auto-round}\n"
    str_result += "\\begin{tabular}{"
    for i in range(ncols):
        # use S column
        str_result += " S["
        # set format
        if fmt[i]:
            str_result += "table-format={},".format(fmt[i])
        if isinstance(content[0][i], UFloat):
            str_result += "table-figures-uncertainty=1,"
        str_result += "]"
    str_result += "}\n\\toprule\n"

    # write top row with title and unit
    for col_idx in range(ncols):
        str_result += "{" + col_title[col_idx] + "} & "
    # delete last "&"
    str_result = str_result[:-3]
    str_result += "\\\\\n\\midrule\n"

    for row_values in content:
        # write values
        for col_idx, val in enumerate(row_values):
            if isinstance(val, UFloat):
                str_result += _ufloat_to_str(val)
            elif isinstance(val, str):
                # brackets for no interpretation by siunitx
                str_result += "{{ {val} }}".format(val=val)
            elif isinstance(val, np.ndarray):
                str_result += str(val).replace('/', '')
            else:
                py_fmt = _python_fmt(fmt[col_idx])
                str_result += "{val:{fmt}}".format(val=val, fmt=py_fmt)
            str_result += " & "
        # delete last "&"
        str_result = str_result[:-3]
        str_result += "\\\\\n"

    str_result += "\\bottomrule\n\\end{tabular}\n\\end{center}\n"
    if table_env:
        str_result += "\\end{table}"

    file_name = os.path.join(output_dir, "tbl_{}.tex".format(name))
    # create output_dir
    if output_dir:
        os.makedirs(os.path.dirname(file_name), exist_ok=True)
    # save to file
    with open(file_name, "w", encoding="utf-8") as file:
        file.write(str_result)
    if(_output_level):
        print("latex_table(): Saved {} to {}".format(name, file_name))


def latex_val(name, val, unit="", digits=None, use_siunitx=True,
              siunitx_options="", output_dir=_output_dir):
    """
    Save a variable to a file in LaTeX-Style.

    Parameters
    ----------
    name : str
        The name of the variable, defines the filename.
        Filename for ``name="x"`` will be `val_x.tex`.
    val : {int, float, ufloat}
        The value to be saved.
    unit : str, optional
        The unit of the value in `siunitx` notation (e.g. "\milli\meter")
    digits : int, optional
        The value is rounded to a specific number of digits after the
        decimal point. No rounding, if no value for ``digits`` is provided.
    use_siunitx : bool, optional
        If true (default), the output value is wrapped by a siunitx
        environment, i.e. \SI{val}{unit} or \num{val}.
    siunitx_options : str, optional
        Additional siunitx arguments can be provided.
    output_dir : str, optional
        The directory, in which the .tex file is saved to.
        Defaults to the global variable ``_output_dir``.

    Notes
    -----
        For more information on siunitx, see
        http://mirror.hmc.edu/ctan/macros/latex/contrib/siunitx/siunitx.pdf
    """
    val = val * 1

    has_uncertainty = isinstance(val, UFloat)

    if has_uncertainty:
        str_val = _ufloat_to_str(val)
    else:
        if digits is not None:
            str_val = "{val:.{digits}f}".format(val=val, digits=digits)
        else:
            str_val = str(val)

    if use_siunitx:
        siunitx_options = "[{}]".format(siunitx_options)
        if unit:
            str_result = "\\SI{opt}{{{val}}}{{{unit}}}".format(
                             opt=siunitx_options, val=str_val, unit=unit)
        else:
            str_result = "\\num{opt}{{{val}}}".format(
                             opt=siunitx_options, val=str_val)
    else:
        str_result = str_val
    # Add brackets around number, if necessary
    if has_uncertainty and (not unit) and (str_val.count("e") == 0):
        str_result = "({})".format(str_result)

    file_name = os.path.join(output_dir, "val_{}.tex".format(name))
    # create output_dir
    if output_dir:
        os.makedirs(os.path.dirname(file_name), exist_ok=True)
    # save to file
    with open(file_name, "w", encoding="utf-8") as file:
        file.write(str_result)
    if(_output_level):
        print("latex_val(): Saved {} {} to {}".format(str_val, unit,
                                                      file_name))
