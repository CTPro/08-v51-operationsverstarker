\input{header.tex}
\input{metadata.tex}
\input{shortcuts.tex}

\newcommand{\val}[1]{\input{build/val_#1}}
\newcommand{\tbl}[1]{\input{build/tbl_#1}}
\newcommand{\UA}{\ensuremath{U_\mathrm{A}}}
\newcommand{\Up}{\ensuremath{U_\mathrm{p}}}
\newcommand{\UN}{\ensuremath{U_\mathrm{N}}}
\newcommand{\UB}{\ensuremath{U_\mathrm{B}}}

\author{
  Tim Kallage    \\    tim.kallage@tu-dortmund.de \and
  Christian Geister \\ christian.geister@tu-dortmund.de
}
\title{(\Versuchsnummer)\\\Versuchstitel}
\date{Durchführung: \Versuchsdatum}

\begin{document}
\maketitle
\thispagestyle{empty}
\vfill
\tableofcontents
\newpage

%------------------------------------------------
\section{Motivation}
%------------------------------------------------
In diesem Versuch wird das Verhalten von Operationsverstärkern
in verschiedenen Schaltungen untersucht.

%------------------------------------------------
\section{Theoretische Grundlagen}
%------------------------------------------------
Ein Operationsverstärker gibt eine Spannung aus, die, innerhalb des Aussteuerungsbereichs
$-\UB < \UA < \UB$, proportional zur Differenz der Eingangsspannungen
$\Up$ und $\UN$ ist:
\begin{equation} \label{eq:UA}
  \UA = V(\Up - \UN)
\end{equation}
Würde $\UA$ den Aussteuerungsbereich verlassen, so geht der Operationsverstärker
in Sättigung $\UA = \pm \UB$.
Nach \eqref{eq:UA} ist $\Up$ in Phase mit der Ausgangsspannung $\UA$ und wird daher als
nicht-invertierender Eingang bezeichnet. Demnach wird $\UN$ als invertierender Eingang
bezeichnet, da diese Spannung gegenphasig zu $\UA$ ist.
Das Schaltbild eine Operationsverstärkers ist in Abb.~\ref{fig:Schaltsymbol} dargestellt.

\begin{figure}[htb]
  \centering
  \includegraphics[width=.5\textwidth]{media/Schaltsymbol.png}
  \caption{Schaltsymbol eines Operationsverstärkers \cite{Anleitung}.
  Im Folgenden werden die Betriebsspannungen $\pm \UB$ weggelassen.}
  \label{fig:Schaltsymbol}
\end{figure}

Ein realer Operationsverstärker besitzt eine frequenzabhängige Leerlaufverstärkung $V \gg 1$.
Diese ist bei einem idealen Operationsverstärker $V_\mathrm{id} = \infty$.
Die Eingangswiderstände $r_\mathrm{e,p}$ und $r_\mathrm{e,N}$ können beim realen Operationsverstärker
sehr große Werte annehmen, für einen idealen Operationsverstärker sind beide unendlich.
Sehr klein ist beim realen Operationsverstärker der Ausgangswiderstand $r_\mathrm{a}$, im idealen Fall
ist dieser null.

Das Verhalten einer Schaltung mit einem idealen Operationsverstärker hängt ausschließlich
vom äußeren Netzwerk ab, was die Berechnung einfacher macht.
Für reale Operationsverstärker, dessen Kenndaten denen des idealen schon sehr nahe kommen,
sind nur kleine Korrektureffekte zu erwarten.

Im Folgenden werden verschiedene Schaltungen diskutiert, die mithilfe eines oder mehreren
Operationsverstärkern realisiert werden können.

\subsection{Linearverstärker}

\begin{figure}[tb]
  \centering
  \includegraphics[width=.5\textwidth]{media/Linearverstaerker.png}
  \caption{Schaltungsbild eines gegengekoppelten Operationsverstärkers als Linearverstärker \cite{Anleitung}.}
  \label{fig:Linearverstaerker}
\end{figure}

Ein Operationsverstärker kann mit einer sogenannten Gegenkopplung als Linearverstärker betrieben werden.
Obwohl bereits ein unmodifizierter Operationsverstärker eine lineare Kennlinie besitzt, ist der
Aussteuerungsbereich durch die hohe Leerlaufverstärkung $V$ sehr schmal.
Indem nun mithilfe eine Widerstandes ein Teil der Spannung $\UA$ wieder auf den invertierenden Eingang $\Up$
gegeben wird, führt eine Erhöhung der Ausgangsspannung zu einer Abnahme der Spannungsdifferenz und damit
wiederum zu einer Abnahme der Ausgangsspannung.
Der Verstärkungsfaktor $V'$ der neuen Schaltung ergibt sich mit den Widerständen $R_1$ und $R_\mathrm{N}$
(vgl. Abb.~\ref{fig:Linearverstaerker}) zu
\begin{equation} \label{eq:V'}
  V' = \frac{R_\mathrm{N}}{R_1} .
\end{equation}
Gleichung~\eqref{eq:V'} gilt nicht mehr exakt, wenn man eine endliche Leerlaufverstärkung annimmt.
Für einen realen Operationsverstärker ergibt sich
\begin{equation} \label{eq:leerlauf}
  \frac{1}{V'} \approx \frac{1}{V} + \frac{R_1}{R_\mathrm{N}} .
\end{equation}
Für $\frac{R_\mathrm{N}}{R_1} << V$ ergibt sich ein kleiner, aber von $V$ (fast) unabhängiger Verstärkungsfaktor
und es ergibt sich wieder \eqref{eq:V'}.
Dies erhöht die Stabilität des Verstärkers gegenüber Abweichungen von $V$, z.B. durch Temperaturschwankungen.
Das Frequenzband, das von dem Verstärker unverzerrt übertragen werden kann, wird um den Faktor
\begin{equation}
  \frac{1}{g} = \frac{V'}{V}
\end{equation}
erhöht, vgl. Abb.~\ref{fig:Frequenzgang}.

\begin{figure}[htb]
  \centering
  \includegraphics[width=.6\textwidth]{media/Frequenzgang.png}
  \caption{Frequenzgang des Linearverstärkers \cite{Anleitung}.}
  \label{fig:Frequenzgang}
\end{figure}

Bei hochohmigen Spannungsquellen kann es durch den geringen Eingangswiderstand zu Verfälschungen
bei der Spannungsmessung kommen.
Ein Linearverstärker ohne diesen Nachteil ist z.B. der Elektrometerverstärker (näheres siehe Ref.~\cite{Anleitung}).

\subsection{Umkehr-Integrator und Umkehr-Differentiator}

Wird der Widerstand $R_\mathrm{N}$ durch einen Kondensator $C$ ersetzt, so erhält man
für die Ausgangsspannung die Beziehung
\begin{equation} \label{eq:Integrator}
  U_\mathrm{A} = - \frac{1}{RC}\int\!\mathrm{d}t \,U_1(t) .
\end{equation}
Das Schaltbild eines Umkehr-Integrators ist in Abb.~\ref{fig:Integrator} zu sehen.

\begin{figure}[htb]
  \centering
  \includegraphics[width=.5\textwidth]{media/Integrator.png}
  \caption{Schaltbild eines Umkehr-Integrators \cite{Anleitung}.}
  \label{fig:Integrator}
\end{figure}

Für eine Sinusspannung der Form
\begin{equation}
  U_1(t) = U_0 \sin\omega t
\end{equation}
folgt also
\begin{equation}
  U_\mathrm{A}(t) = \frac{U_0}{\omega RC}\cos\omega t .
\end{equation}
Es ergibt sich also eine Proportionalität der Ausgangsspannung zu $\omega^{-1}$.

Durch das Tauschen des Widerstands mit der Kapazität wird die Ausgangsspannung differenziert.
\begin{equation} \label{eq:Differentiator}
  U_\mathrm{A}(t) = -RC\frac{\mathrm{d}U_1}{\mathrm{d}t}
\end{equation}
Eine solche Schaltung wird als Umkehr-Differentiator bezeichnet.
Das Schaltbild ist in Abb.~\ref{fig:Differentiator} dargestellt.

\begin{figure}[htb]
  \centering
  \includegraphics[width=.5\textwidth]{media/Differentiator.png}
  \caption{Schaltbild eines Umkehr-Differentiators \cite{Anleitung}.}
  \label{fig:Differentiator}
\end{figure}

Hier ergibt sich mit der gleichen Eingangsspannung wie beim Integrator,
\begin{equation}
  U_\mathrm{A}(t) = - \omega R C U_0 \cos\omega t,
\end{equation}
also eine Proportionalität zwischen $\omega$ und $U_\mathrm{A}$.

\subsection{Schmitt-Trigger}

Nun wird eine Schaltung betrachtet, bei der ein Teil der Ausgangsspannung
auf den nicht-invertierenden Eingang gegeben wird.
Dies sorgt für eine Mitkopplung, ein Anstieg von $\UA$ bewirkt also ein Anstieg der
Spannungsdifferenz der Eingänge und nach \eqref{eq:UA} wiederum einen Anstieg von $\UA$.
Das Schaltbild einer solchen Schaltung ist in Abb.~\ref{fig:Schmitt-Trigger} dargestellt.

\begin{figure}[htb]
  \centering
  \includegraphics[width=.5\textwidth]{media/Schmitt-Trigger.png}
  \caption{Schaltbild eines Schmitt-Triggers \cite{Anleitung}.}
  \label{fig:Schmitt-Trigger}
\end{figure}

Die Ausgangsspannung springt jeweils auf $\pm \UB$, sobald die Spannung $U_1$
die Schwelle
\begin{equation} \label{eq:schmitt}
  U_{1,\mathrm{Schwelle}} = \pm \frac{R_1}{R_\mathrm{p}}\UB
\end{equation}
erreicht.
Die Differenz zwischen der positiven und negativen Schwelle,
$2 \UB R_1 / R_\mathrm{p}$ wird als Schalthysterese bezeichnet.

\subsection{Signalgenerator}

\begin{figure}[htb]
  \centering
  \includegraphics[width=.8\textwidth]{media/Signalgenerator1.png}
  \caption{Schaltbild eines Dreieck- und Rechteckgenerators \cite{Anleitung}.}
  \label{fig:Signalgenerator1}
\end{figure}

Wird ein Integrator hinter einen Schmitt-Trigger geschaltet,
wie in Abb.~\ref{fig:Signalgenerator1} dargestellt, so wird die konstante
Ausgangsspannung des Schmitt-Triggers integriert und die Ausgangsspannung
des zweiten Operationsverstärkers fällt nach \eqref{eq:Integrator} linear ab.
Diese Ausgangsspannung wird wiederum auf den Eingang $U_1$ des Schmitt-Triggers
geschaltet, dessen Ausgangspannung auf $-\UB$ abfällt, sobald $U_1$ unter die
Schwelle $-\UB R_1 / R_\mathrm{p}$ fällt.
Diese konstante Spannung wird wiederum integriert und kippt die Triggerspannung
wieder um, sobald die obere Schwelle erreicht ist.
An dem Ausgang des Schmitt-Trigger kann nun also eine Rechteck-Spannung abgegriffen werden,
während am Ausgang des Integrators eine Dreieck-Spannung erzeugt wird.
Die Periodendauer dieser Spannungen hängt von dem Widerstandsverhältnis $R_1 / R_\mathrm{p}$
des Schmitt-Triggers, sowie der Integratorzeitkonstanten $RC$ ab.

\subsection{Sinusgenerator mit zeitlich veränderlicher Amplitude}

Mittels zweier Umkehr-Integratoren und eines Umkehrverstärkers, die wie in
Abb.~\ref{fig:Signalgenerator2} dargestellt geschaltet sind, lassen sich
exponentiell mit der Zeit zu- oder abnehmende Sinusschwingungen erzeugen.
\begin{figure}[htb]
  \centering
  \includegraphics[width=.8\textwidth]{media/Signalgenerator2.png}
  \caption{Schaltbild eines Sinusgenerators mit zeitlich veränderlicher
  Amplitude \cite{Anleitung}.}
  \label{fig:Signalgenerator2}
\end{figure}

Es wird ein Parameter $\eta$ eingeführt, der dem Anteil entspricht, die von der
Ausgangsspannung $\UA$ wieder auf den Eingang des Operationsverstärkers OV2
gegeben wird.
Er kann durch das Potentiometer im Wertebereich $-1 \leq \eta \leq 1$ variiert werden.
Für $U_1$ und $U_2$ erhält man
\begin{align}
  U_1 &= -\frac{1}{RC}\int\!\mathrm{d}t\,\UA, &
  U_2 &= -\frac{1}{RC}\int\!\mathrm{d}t\,\left(U_1 + \frac{1}{10}\eta\UA\right),
\end{align}
und für den Ausgang des Umkehrverstärkers
\begin{equation}
  \UA = - U_2.
\end{equation}
Aus den obigen Gleichungen lassen sich $U_1$ und $U_2$ eliminieren und man erhält
nach zweifacher zeitlicher Ableitung
\begin{equation}
  \frac{\mathrm{d}^2\UA}{\mathrm{d}t^2} - \frac{\eta}{10RC}\frac{\mathrm{d}\UA}{\mathrm{d}t}
  + \frac{1}{(RC)^2} \UA = 0 .
\end{equation}
Mit der hier erfüllten Bedingung $\eta^2 \ll 100$ ergibt sich die Lösung dieser
Differentialgleichung zu
\begin{equation}
  \UA(t) = U_0\exp\left(\frac{\eta t}{20 RC}\right)\sin\left(\frac{t}{RC}\right),
\end{equation}
also einer gedämpften ($\eta < 0$) oder entdämpften ($\eta > 0$) Sinusschwingung
mit der Schwingungsdauer
\begin{equation} \label{eq:periode}
  T = 2\pi RC
\end{equation}
und der Abkling- bzw. Zunahmedauer
\begin{equation} \label{eq:abklingdauer}
  \tau = \frac{20RC}{|\eta|} .
\end{equation}
Im Fall $\eta = 0$ bleibt die Amplitude der Sinusschwingung konstant.

%------------------------------------------------
\section{Aufbau und Durchführung}
%------------------------------------------------
\begin{enumerate}
\item[a)]
  Im ersten Versuchsteil wird die Frequenzabhängigkeit des Verstärkungsgrades $V'$
  untersucht.
  Dazu werden vier Messreihen mit verschiedenen Widerstandsverhältnissen $R_\mathrm{N}/R_1$
  durchgeführt und die Frequenz der Wechselspannung über mehrere Zehnerpotenzen variiert.
\item[b)]
  Danach wird zuerst eine Integratorschaltung aufgebaut und für eine Sinusspannung
  die Beziehung $\UA \propto \omega^{-1}$ untersucht.
  Im Anschluss wird jeweils eine Sinus-, Dreieck- und Rechteckspannung zusammen mit der
  entstehenden Ausgangsspannung $\UA$ auf einem Oszilloskop qualitativ untersucht.
  Für eine Differentiatorschaltung wird wieder zuerst die proportionalität von $\UA$ und
  $\omega$ untersucht und danach die drei verschiedenen Signalformen zusammen mit $\UA$
  auf dem Oszilloskop untersucht.
\item[c)]
  Nun wird die Schwellenspannung eines Schmitt-Triggers untersucht indem die Amplitude
  einer Sinusspannung solange erhöht wird, bis die Ausgangsspannung anfängt zu kippen.
\item[d)]
  Die Schaltung eines Dreieckgenerators wird aufgebaut und die entstehende Spannung
  auf einem Oszilloskop analysiert, sowie die Frequenz und Amplitude untersucht.
\item[e)]
  Als letztes wird eine Sinusgeneratorschaltung realisiert und die entstehende
  Ausgangsspannung für verschiedene Werte von $\eta$ auf einem Oszilloskop untersucht.
  Desweiteren wird die Schwingungsfrequenz von $\UA$ aufgenommen.
\end{enumerate}

%------------------------------------------------
\section{Auswertung}
\subsection{Gegengekoppelter Verstärker}
Der Verstärkungsgrad $V'$ ist gegeben durch das Verhältnis der Ausgangsspannung $U_A$ zur Eingangsspannung $U_1$.

Der ideale Verstärkungsfaktor ergibt sich aus Gleichung (\ref{eq:V'}).
Die Leerlaufverstärkung $V$ kann aus Gleichung (\ref{eq:leerlauf}) bestimmt werden.

Für die Grenzfrequenz $\nu_g$ gilt die Beziehung
\begin{equation}
V(\nu_g)=\frac{V'}{\sqrt{2}}.
\end{equation}

Die Messdaten für vier verschiedene Verstärkungsgerade sind in den Abbildungen \ref{fig:v1} bis \ref{fig:v4} zu finden.
Durch lineare Regression wird die Verstärkung $V'$ bei $\nu=0$ bestimmt, sowie die Parameter für die zweiter Gerade, die zur Bestimmung der Grenzfrequenz genutzt wird.
Die Ergebnisse sind in Tabelle \ref{tbl:gegengekoppelt} zusammengefasst.
Da beim potenzieren die Fehler von $\nu_g$ und dem Produkt mit $V'$ stark vergrößert werden, wird für diese Größen nur der Logarithmus betrachtet.  

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{build/V1.pdf}
\caption{Frequenzgang $V_1$.}
\label{fig:v1}
\end{figure}
\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{build/V2.pdf}
\caption{Frequenzgang $V_2$.}
\end{figure}
\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{build/V3.pdf}
\caption{Frequenzgang $V_3$.}
\end{figure}
\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{build/V4.pdf}
\caption{Frequenzgang $V_4$.}
\label{fig:v4}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{build/phase.pdf}
\caption{Phasendifferenz zwischen Eingangs- und Ausgangssignal eines Operationsverstärkers.}
\label{fig:phase}
\end{figure}

Die Phase der Ein- und Ausgangsspannung zueinander ist in Abbildung \ref{fig:phase} dargestellt.
Für niedrige Frequenzen liegt dies konstant um \SI{180}{\degree} und wächst mit zunehmender Frequenz an.

\begin{table}[ht]
\begin{center}
\caption{Berechnete Größen des Gegengekoppelten Verstärkers für verschiedene Verstärkungsgrade.}
\label{tbl:gegengekoppelt}
\sisetup{table-auto-round}
\begin{tabular}{
S[table-format=1.2,]
S[table-format=2.2,]
S[table-format=2.1,]
S[table-format=1.2,table-figures-uncertainty=1,]
S[table-format=2.1,table-figures-uncertainty=1,]
S[table-format=2.1,table-figures-uncertainty=1,]
S[table-format=4.0,table-figures-uncertainty=1,]}
\toprule
{$R_1$ [\si{\kilo\ohm}]} & {$R_N$ [\si{\kilo\ohm}]} & {$V_{id}$} & {$V'$} & {$\ln\nu_g$ [\si{\hertz}]} & {$\ln(V'\nu_g)$ [\si{\hertz}]} & {$V$}\\
\midrule
0.47 &  1.00 &  2.1 & 1.92+-0.05 & 12.5+-1.0 & 13.1+-1.0 & 20+-5\\
9.95 & 33.10 &  3.3 & 3.12+-0.10 & 12.5+-0.9 & 13.6+-0.9 & 51+-26\\
9.95 & 99.50 & 10.0 & 9.94+-0.03 & 11.3+-0.4 & 13.5+-0.4 & 1800+-800\\
9.95 &  9.95 &  1.0 & 0.98+-0.01 & 13.1+-1.1 & 13.0+-1.1 & 60+-40\\
\bottomrule
\end{tabular}
\end{center}
\end{table}

\FloatBarrier
\subsection{Umkehrintegrator und Umkehrdifferentiator}
Ein Operationsverstärker mit entsprechender Beschaltung kann als Integrator bzw. Differenziator verwerdent werden.
Dabei ist die Ausgangsspannung von der Frequenz abhängig.
In einem doppel-logarithmischen Diagramm ergibt sich ein linearer Zusammenhang, der in Abbildung \ref{fig:integrator} bzw. \ref{fig:differentiator} dargestellt ist.
\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{build/integrator.pdf}
\caption{Ausgangsspannung beim Integrator.}
\label{fig:integrator}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{build/differentiator.pdf}
\caption{Ausgangsspannung beim Differentiator.}
\label{fig:differentiator}
\end{figure}

Verschiedene Integrierte Signale sind in Abbildung \ref{fig:int_plots} und verschiedene differenzierte Signale in Abbildung \ref{fig:dif_plots} dargestellt.

\begin{figure}
\centering
\includegraphics[width=0.49\textwidth]{data/sinus_int.png}
\includegraphics[width=0.49\textwidth]{data/rechteck_int.png}
\includegraphics[width=0.49\textwidth]{data/dreieck_int.png}
\caption{Sinus-, Rechteck- und Dreieckspannung in blau mit den negativen integrierten Spannungen in lila.}
\label{fig:int_plots}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.49\textwidth]{data/sinus_dif.png}
\includegraphics[width=0.49\textwidth]{data/rechteck_dif.png}
\includegraphics[width=0.49\textwidth]{data/dreieck_dif.png}
\caption{Sinus-, Rechteck- und Dreieckspannung in blau mit den negativen differenzierten Spannungen in lila.}
\label{fig:dif_plots}
\end{figure}


\FloatBarrier
\subsection{Schmitt-Trigger}
Der theoretische Wert der Ausgangsspannung eines Schmitt-Triggers berechnet sich nach Gleichung (\ref{eq:schmitt}) zu \SI{99}{\milli\volt}.
Die verwendeten Werte sind
\begin{align*}
R_P &= \SI{9.95}{\kilo\ohm}\\
R_1 &= \SI{100}{\ohm}\\
U_B &= \SI{19.8}{\volt}.
\end{align*}
Die gemessene Schwellspannung beträgt
\begin{equation}
\SI{117}{\milli\volt}.
\end{equation}

\subsection{Generator für Dreiecksspannung}
Um die Frequenz eines Generators für Dreiecksspannung zu berechnen, wird die Schaltung \ref{fig:Signalgenerator1} betrachtet.
Der Integrationsteil liefert
\begin{equation}
U_A = \frac{1}{RC}\int_0^{T/2} U_B \mathrm{d}t = \frac{T}{2RC}U_B.
\end{equation}
Es gilt $U_A = U_1$ und mit der Ausgangsspannung für den Schmitt-Trigger ergibt sich
\begin{align}
U_A = U_1 &= \frac{R_1}{R_P}U_B\\
\frac{T}{2RC}U_B &= \frac{R_1}{R_P}U_B\\
\Rightarrow T &= \frac{2R_1 RC}{R_P}.
\end{align}
Mit $R=\SI{1}{\kilo\ohm}$ und $C=\SI{22}{\micro\farad}$ errechnet sich der theoretische Wert
\begin{equation}
\nu = \SI{2.26}{\kilo\hertz}.
\end{equation}
Gemessen wurde
\begin{equation}
\nu_{\text{exp}} = \SI{2.1}{\kilo\hertz}.
\end{equation}

\subsection{Sinusgenerator}
Nach Gleichung (\ref{eq:periode}) liefert der Sinusgenerator aus Operationsverstärkern eine theoretische Frequenz von 
\begin{equation}
\nu = \frac{1}{2\pi R C} = \SI{723}{\hertz}
\end{equation}
mit den Werten
\begin{align*}
R = \SI{10}{\kilo\ohm}\\
C = \SI{22}{\nano\farad}.
\end{align*}
Die gemessene Frequenz beträgt
\begin{equation}
\SI{633}{\hertz}.
\end{equation}

Das Abklingverhalten für $\eta = -1$ ist in Abbildung \ref{fig:abklingdauer} dargestellt.
Die Abklingdauer berechnet sich nach Formel (\ref{eq:abklingdauer}) zu 
\begin{equation}
\SI{0.0044}{\per\second}.
\end{equation}
Aus dem Fit in Abbildung \ref{fig:abklingdauer_fit} ergibt sich eine Steigung von $\num{-260+-7}$ und damit eine Abklingdauer von
\begin{equation}
\SI{0.0038+-0.0001}{\per\second}.
\end{equation}

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{data/abklingdauer.png}
\caption{Abklingen des Sinusgenerators.}
\label{fig:abklingdauer}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{build/abklingdauer.pdf}
\caption{Maxima der abklingenden Sinusspannung im zeitlichen Verlauf.}
\label{fig:abklingdauer_fit}
\end{figure}


%------------------------------------------------
\section{Diskussion}
%------------------------------------------------
Die bestimmten Verstärkungsgerade sind systematisch etwas geringer als die theoretischen Werte.
Dies könnte an der Leerlaufverstärkung oder auch an den vernachlässigten Widerständen in Leitungen und Steckern liegen.
Das Produkt aus Grenzfrequenz und Verstärkungsfaktor ist im Rahmen der Standardabweichung konstant.
Das Abfallen der Verstärkung bei hohen Frequenzen lässt sich auch mit einem Tiefpass erreichen.

Der Integrator und Differentiator zeigen in dem Untersuchten Frequenzbereich das theoretisch beschriebene Verhalten.

Die gemessene Schwellspannung des Schmitt-Triggers weicht um 17\% von dem berechneten Wert ab.
Fertigungstoleranzen der Bauelemente sowie thermische Beeinflussung können her das Ergebnis verfälschen.

Bei der Erzeugung von Dreiecksspannung weicht die Frequenz nur um 7\% vom theoretischen Wert ab.

Die Frequenz der erzeugten Sinusspannung weicht um 12\% von der Theorie ab.
Bei der Abklingdauer von \SI{0.0038+-0.0001}{\per\second} liegt wahrscheinlich ein zu kleiner Fehler vor, da Messunsicherheiten unterschätzt wurden. Eventuell ergibt sich eine Verbesserung, wenn ein größeres Intervall gemessen wird.

%------------------------------------------------
% LITERATURVERZEICHNIS
%------------------------------------------------
\begin{thebibliography}{xx}
\small
\bibitem{Anleitung}
	TU Dortmund,
	\textit{Versuch \Versuchsnummer~- \Versuchstitel},
	\url{http://129.217.224.2/HOMEPAGE/Anleitung_FPMa.html},
	abgerufen am \Versuchsdatum.

\end{thebibliography}

\end{document}
