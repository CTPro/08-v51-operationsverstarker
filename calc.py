import numpy as np
import matplotlib.pyplot as plt
import latexoutput as lout
print("Using latexoutput, Version", lout.__version__)

"""comment out what you need"""
#import matplotlib as mpl
#mpl.rc('axes', {'formatter.useoffset': False, 'formatter.limits': [-5, 5]})
from scipy.optimize import curve_fit
#from scipy.stats import sem # standard error of mean
from uncertainties import ufloat, ufloat_fromstr
import uncertainties.unumpy as unp
#from uncertainties.unumpy import (nominal_values as noms, std_devs as stds)
#import scipy.constants as const
#from scipy.interpolate import interp1d
#from scipy.integrate import simps, quad, trapz

def f(x, a, b):
    return a * x + b


# Gegenkopplung
fq_1, U_A_1, U_1_1 = np.genfromtxt("data/V1.txt", unpack=True)
fq_2, U_A_2, U_1_2, phi = np.genfromtxt("data/V2.txt", unpack=True)
fq_3, U_A_3, U_1_3 = np.genfromtxt("data/V3.txt", unpack=True)
fq_4, U_A_4, U_1_4 = np.genfromtxt("data/V4.txt", unpack=True)


name = ["V1", "V2", "V3", "V4"]
fq = [fq_1, fq_2, fq_3, fq_4]
U_A = [U_A_1, U_A_2, U_A_3, U_A_4]
U_1 = [U_1_1, U_1_2, U_1_3, U_1_4]
R_1 = [470, 9.95e3, 9.95e3, 9.95e3]
R_N = [1e3, 33.1e3, 99.5e3, 9.95e3]
V_quer_end = [4, 4, 4, 4]
V_abfall_start = [7, 7, 7, 7]

V = [0, 0, 0, 0]
V_prime = [0, 0, 0, 0]
nu_g = [0, 0, 0, 0]
V_id = [0, 0, 0, 0]
V_prime_nu_g = [0, 0, 0, 0]
V_leer = [0, 0, 0, 0]

for i in range(4):

	V[i] = U_A[i] / U_1[i]



	x = np.log(fq[i])
	y = np.log(V[i])

	par, cov = curve_fit(f, x[:V_quer_end[i]], y[:V_quer_end[i]])
	err = np.sqrt(np.diag(cov))

	fit_a = ufloat(par[0], err[0])
	V_prime[i] = ufloat(par[1], err[1])
	par_f = par

	par, cov = curve_fit(f, x[V_abfall_start[i]:], y[V_abfall_start[i]:])
	err = np.sqrt(np.diag(cov))

	fit2_a = ufloat(par[0], err[0])
	fit2_b = ufloat(par[1], err[1])
	par2_f = par

	x_offset = (np.max(x) - np.min(x)) * 0.05
	x_plot = np.linspace(np.min(x) - x_offset, np.max(x) + x_offset, 100)

	x_offset2 = (np.max(x[V_abfall_start[i]:]) - np.min(x[V_abfall_start[i]:])) * 0.05
	x_plot2 = np.linspace(np.min(x[V_abfall_start[i]:]) - x_offset, np.max(x[V_abfall_start[i]:]) + x_offset, 100)

	fig = plt.figure()
	# Einfacher Plot
	plt.plot(x[:V_quer_end[i]], y[:V_quer_end[i]], 'x', label='Messdaten für $V\'$')
	plt.plot(x[V_quer_end[i]:V_abfall_start[i]], y[V_quer_end[i]:V_abfall_start[i]], 'x', label='Messdaten')
	plt.plot(x[V_abfall_start[i]:], y[V_abfall_start[i]:], 'x', label='Messdaten für $\\nu_g$')
	# Plot einer Ausgleichs- oder Theoriefunktion f
	plt.plot(x_plot, f(x_plot, *par_f), '-', label='Fit $V\'$')
	plt.plot(x_plot2, f(x_plot2, *par2_f), '-', label='Fit $\\nu_g$')
	plt.xlabel('Frequenz $\\ln(\\nu)$ in Hz')
	plt.ylabel('Verstärkungsfaktor $\\ln(V)$')
	#plt.xlim(np.min(x_plot), np.max(x_plot))
	plt.legend(loc='best')
	plt.savefig('build/' + name[i] + '.pdf')
	plt.close()

	V_prime[i] = unp.exp(V_prime[i])
	nu_g[i] = (unp.log(V_prime[i] / np.sqrt(2)) - fit2_b) / fit2_a
	V_prime_nu_g[i] = unp.log(V_prime[i]) + nu_g[i]

	V_leer[i] = 1 / (1 / V_prime[i] - (R_1[i] / R_N[i]))
	V_id[i] = R_N[i] / R_1[i]
	R_N[i] *= 1e-3
	R_1[i] *= 1e-3


lout.latex_table(name = 'gegengekoppelt',
    content = [R_1,R_N,V_id,V_prime,nu_g,V_prime_nu_g,V_leer],
    col_title = '$R_1$, $R_N$, $V_{id}$, $V\'$, $\\ln\\nu_g$,$\\ln(V\'\\nu_g)$,$V$',
    col_unit = r'\kilo\ohm,\kilo\ohm,,,\hertz,\hertz',
    fmt = '1.2,2.2,2.1,2.2,2.1,2.1,4.0',
    caption = r'Berechnete Größen des Gegengekoppelten Verstärkers.')



fig = plt.figure()
plt.plot(np.log(fq_2), phi, 'x', label='Messdaten')
plt.xlabel('Frequenz $\\ln(\\nu)$ in Hz')
plt.ylabel('Phase in Grad')
plt.legend(loc='best')
plt.savefig('build/phase.pdf')
plt.close()

#integrator
fq, U_A, U_0 = np.genfromtxt("data/teil_d.txt", unpack=True)

x = np.log(fq)
y = np.log(U_A/U_0)

par, cov = curve_fit(f, x, y)
err = np.sqrt(np.diag(cov))

fit_a = ufloat(par[0], err[0])
fit_b = ufloat(par[1], err[1])
par_f = par

x_offset = (np.max(x) - np.min(x)) * 0.05
x_plot = np.linspace(np.min(x) - x_offset, np.max(x) + x_offset, 100)

fig = plt.figure()
plt.plot(x, y, 'x', label='Messdaten')
plt.plot(x_plot, f(x_plot, *par_f), '-', label='Fit')
plt.xlabel('Frequenz $\\ln(\\nu)$ in Hz')
plt.ylabel('Ausgangsspannung $\\ln(U_A/U_0)$')
#plt.xlim(np.min(x_plot), np.max(x_plot))
plt.legend(loc='best')
plt.savefig('build/integrator.pdf')
plt.close()


#differentiator
fq, U_A, U_0 = np.genfromtxt("data/teil_e.txt", unpack=True)

x = np.log(fq)
y = np.log(U_A/U_0)

par, cov = curve_fit(f, x, y)
err = np.sqrt(np.diag(cov))

fit_a = ufloat(par[0], err[0])
fit_b = ufloat(par[1], err[1])
par_f = par

x_offset = (np.max(x) - np.min(x)) * 0.05
x_plot = np.linspace(np.min(x) - x_offset, np.max(x) + x_offset, 100)

fig = plt.figure()
plt.plot(x, y, 'x', label='Messdaten')
plt.plot(x_plot, f(x_plot, *par_f), '-', label='Fit')
plt.xlabel('Frequenz $\\ln(\\nu)$ in Hz')
plt.ylabel('Ausgangsspannung $\\ln(U_A/U_0)$')
#plt.xlim(np.min(x_plot), np.max(x_plot))
plt.legend(loc='best')
plt.savefig('build/differentiator.pdf')
plt.close()

#schmitt-trigger
R_1 = 100
R_P = 9950
U_B = 19.8 / 2
U_1 = (R_1 / R_P) * U_B
print("schmitt")
print(U_1)

#dreieck
R_1 = 100
R_P = 9950
R = 1000
C = 22e-6
fq = R_P / (2 * R_1 * R * C)
print("dreieick fq")
print(fq)

#sinus
R = 10000
C = 22e-9
nu = 1 / (2*np.pi*R*C)
print("sinus nu")
print(nu)


#abklingdauer
t, ch1, ch2 = np.genfromtxt("data/sinus.txt", unpack=True)
x = t
y = np.log(ch2)

par, cov = curve_fit(f, x, y)
err = np.sqrt(np.diag(cov))

fit_a = ufloat(par[0], err[0])
fit_b = ufloat(par[1], err[1])
par_f = par

x_offset = (np.max(x) - np.min(x)) * 0.05
x_plot = np.linspace(np.min(x) - x_offset, np.max(x) + x_offset, 100)

fig = plt.figure()
plt.plot(x, y, 'x', label='Messdaten')
plt.plot(x_plot, f(x_plot, *par_f), '-', label='Fit')
plt.xlabel('Zeit $t$ in s')
plt.ylabel('Ausgangsspannung $\\ln(U_A/U_0)$')
#plt.xlim(np.min(x_plot), np.max(x_plot))
plt.legend(loc='best')
plt.savefig('build/abklingdauer.pdf')
plt.close()

tau = 20 * R * C
print("tau")
print(tau)
print("fit")
print(fit_a)
print("tau fit")
print(1 / fit_a)