# Makefile für Praktikumsprotokoll
# author: Tim Kallage
# Version: 2017-07-09

BUILDDIR = build
DATADIR = data
MEDIADIR = media
NR = $(shell grep 'Versuchsnummer' metadata.tex | cut -d { -f 3 | cut -d } -f 1)

# Alle Dateien im data-Ordner
DATAFILES = $(wildcard $(DATADIR)/*)
# Alle Dateien im media-Ordner
MEDIAFILES = $(wildcard $(MEDIADIR)/*)
# Alle Python-Dateien
PYSRC = $(wildcard *.py)
# Zu erzeugende .pymake-Dateien
PYOUT = $(patsubst %.py,$(BUILDDIR)/.pymake%,$(PYSRC))
# Haupt-TeX-Datei
MAINTEX = main.tex
# Alle TeX-Dateien
LATEXSRC = $(wildcard *.tex)
# Zu erzeugende PDF
PDF = Protokoll_$(NR).pdf
PDFOUT = $(BUILDDIR)/$(PDF)
# Zu benutzende LaTeX-Engine (latexmk empfohlen)
LATEX = latexmk -pdf --output-directory=$(BUILDDIR) --synctex=1 --interaction=nonstopmode --halt-on-error --jobname=Protokoll_$(NR)
# Zu benutzende Python-Engine
PY = python
# Löschbefehl
RM  = rm -rf
# Temporäre Dateien, die mit 'make clean' gelöscht werden
GARBAGE = $(BUILDDIR) __pycache__

all: $(PYOUT) $(PDFOUT)

py: $(PYOUT)

pvc:
	@echo "Continously building $@"
	@$(LATEX) $(MAINTEX) -pvc -view=none

pdf: $(PDFOUT)

$(PDFOUT): $(LATEXSRC) $(MEDIAFILES) FORCE | $(BUILDDIR) $(MEDIADIR)
	@echo "Building $@"
	@$(LATEX) $(MAINTEX)

$(BUILDDIR)/.pymake%: %.py $(PYSRC) $(DATAFILES) | $(BUILDDIR) $(DATADIR)
	@echo "Running $<"
	@$(PY) $<
	@touch $@

$(MEDIADIR) $(DATADIR) $(BUILDDIR):
	@echo "Creating $@/"
	@mkdir -p $@
	@touch $@/.empty

clean :
	@echo "Cleaning"
	@$(RM) $(GARBAGE)

FORCE:

.PHONY: all clean fast py pdf pvc FORCE
